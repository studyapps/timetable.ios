//
//  BaseCoordinator.swift
//  MDW
//
//  Created by Metalluxx on 14/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

open class BaseCoordinator: Coordinatable {

    // MARK: - Control coordinator
    public var finishFlow:( () -> Void )?

    open func start() {
        fatalError("This method is dont overrided")
    }
    deinit {
        withUnsafePointer(to: self) {
            debugPrint("Dealloc \(self) \($0)")
        }
    }

    // MARK: - Route vars
    public let router: Routable
    public private(set) var childCoordinators: [Coordinatable] = []
    public weak var parentCoordinator: Coordinatable?

    // MARK: - Manage navigationController sharing in coordinators 
//    public var currentNavigationController:UINavigationController?
//    public var navigationController:UINavigationController? {
//        get {
//            guard let currentNavigationController = self.currentNavigationController else {
//                return parentCoordinator?.navigationController
//            }
//            return currentNavigationController
//        }
//        set {
//            currentNavigationController = newValue
//        }
//    }

    // MARK: - Init
    public init(router: Routable, parent: BaseCoordinator? = nil) {
        self.router = router
        self.parentCoordinator = parent
        finishFlow = { [weak parentCoordinator, weak self] in
            guard let parentCoordinator = parentCoordinator as? BaseCoordinator else { return }
            parentCoordinator.removeDependency(self)
        }
        if let parentCoordinator = parentCoordinator as? BaseCoordinator {
            parentCoordinator.addDependency(self)
        }
    }

    // MARK: - Control dependencies
    public func addDependency(_ coordinator: Coordinatable) {
        for element in childCoordinators where element === coordinator {
            return
        }
        childCoordinators.append(coordinator)
    }
    public func removeDependency(_ coordinator: Coordinatable?) {
        guard
            childCoordinators.isEmpty == false,
            let coordinator = coordinator
            else { return }

        for (index, element) in childCoordinators.enumerated() where element === coordinator {
            childCoordinators.remove(at: index)
            break
        }
    }
}
