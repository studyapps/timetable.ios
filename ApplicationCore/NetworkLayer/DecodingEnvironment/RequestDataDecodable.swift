//
//  RequestDataDecodable.swift
//  ApplicationService
//
//  Created by Metalluxx on 29/09/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation

public protocol TypeObjectInResponse{
    @discardableResult func dataRequest<T: Decodable>
        (_ route: DecodableAPIStructure, objectType: T.Type, completion: @escaping (Result<T, Error>, URLResponse?) -> Void)
        -> URLSessionDataTask?
}

public protocol DecodableAPIStructure: APIStructure {
    var keyDecodingStrategy: JSONDecoder.KeyDecodingStrategy { get }
}


