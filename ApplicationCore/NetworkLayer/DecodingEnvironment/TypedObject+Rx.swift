//
//  TypedObject+Rx.swift
//  ApplicationCore
//
//  Created by Metalluxx on 06/10/2019.
//  Copyright © 2019 Metallixx. All rights reserved.
//

import Foundation
import RxSwift


extension Reactive where Base: TypedGateway {
    func dataRequest<T: Decodable>
        (_ route: DecodableAPIStructure, objectType: T.Type)
        -> Single<T>
    {
        return Single<T>.create { (event) -> Disposable in
            self.base.dataRequest(route, objectType: T.self) { (res, _) in
                switch res {
                case .success(let object): event(.success(object))
                case .failure(let error): event(.error(error))
                }
            }
            return Disposables.create()
        }
    }
}


