//
//  DefaultGateway+RDD.swift
//  ApplicationService
//
//  Created by Metalluxx on 29/09/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation


class TypedGateway: AnyGateway, TypeObjectInResponse {
        @discardableResult public func dataRequest<T: Decodable>
        (_ route: DecodableAPIStructure, objectType: T.Type, completion: @escaping (Result<T, Error>, URLResponse?) -> Void)
        -> URLSessionDataTask?
    {
        self.dataRequest(route) { (result, response) in
            switch result {
            case .success(let date):
                do {
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = route.keyDecodingStrategy
                    let object = try decoder.decode(T.self, from: date)
                    completion(.success(object), response)
                } catch let error {
                    completion(.failure(error), response)
                }
            case .failure(let error):
                completion(.failure(error), response)
            }
        }
    }
}


