//
//  DefaultGateway+Rx.swift
//  ApplicationCore
//
//  Created by Metalluxx on 06/10/2019.
//  Copyright © 2019 Metallixx. All rights reserved.
//

import Foundation
import RxSwift

extension AnyGateway: ReactiveCompatible {}

extension Reactive where Base: AnyGateway {
    public func dataRequest(_ route: APIStructure) -> Single<Data> {
        return Single<Data>.create { (event) -> Disposable in
            self.base.dataRequest(route) { (res, _) in
                switch res {
                case .success(let data): event(.success(data))
                case .failure(let error): event(.error(error))
                }
            }
            return Disposables.create()
        }
    }
}





