//
//  NetworkRouter.swift
//  MDW
//
//  Created by Metalluxx on 02/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

extension DefaultGateway: RequestExecutable {
    // MARK: - Create requests
    @discardableResult
    open func downloadRequest(
        _ route: API,
        completion: @escaping (Result<URL, Error>, URLResponse?) -> Void
    ) -> URLSessionDownloadTask?
    {
        var task: URLSessionDownloadTask?
        do {
            task = URLSession.shared.downloadTask(with: try buildRequest(from: route))
            {
                (url, response, error) in
                self.remove(task: task)
                guard let url = url else {
                    completion(.failure(NetworkResponse.noData), response)
                    return
                }
                completion(.success(url), response)
            }
        } catch let error {
            completion(.failure(error), nil)
        }
        
        guard let buildedTask = task else { return nil }
        tasks.append(buildedTask)
        buildedTask.resume()
        return buildedTask
    }

    @discardableResult
    open func dataRequest(
        _ route: API,
        completion: @escaping (Result<Data, Error>, URLResponse?) -> Void
    ) -> URLSessionDataTask?
    {
        var task: URLSessionDataTask?
        do {
            let request = try self.buildRequest(from: route)
            task = URLSession.shared.dataTask(with: request)
            {
                (data, response, error) in
                self.remove(task: task)
                guard let data = data else {
                    completion(
                        .failure(NetworkResponse.noData),
                        response
                    )
                    return
                }
                completion(.success(data), response)
            }
        } catch {
            completion(.failure(error), nil)
        }

        guard let buildedTask = task else { return nil }
        tasks.append(buildedTask)
        buildedTask.resume()
        return buildedTask
    }
}


public class DefaultGateway<API: APIStructure> {
    
    private(set) var tasks = [URLSessionTask]()

    deinit {
        for item in tasks {
            item.cancel()
        }
    }
    
    public init() {}

    // MARK: - Manage tasks
    @discardableResult
    private func remove(task: URLSessionTask?) -> Bool {
        guard let task = task else { return false }

        for (index, item)
            in tasks.enumerated()
            where item == task
        {
            tasks.remove(at: index)
            return true
        }
        return false
    }

    public func cancel(task: URLSessionTask?) {
        guard let task = task else { return }
        remove(task: task) ? task.cancel() : nil
    }

    // MARK: - Manage and build requests
    private func buildRequest(from route: API) throws -> URLRequest {
        
        guard var component = URLComponents(
            url: route.baseURL.appendingPathComponent(route.path),
            resolvingAgainstBaseURL: false)
        else {
            throw NetworkError.missingURL
        }
        
        component.queryItems = route.urlParams?.queryItems
        
        guard let url = component.url else { throw NetworkError.missingURL }
        
        var request = URLRequest(
            url: url,
            cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
            timeoutInterval: 10.0
        )
        request.httpMethod = route.httpMethod.rawValue
        self.addAdditionalHeaders(["Content-Type": route.contentType.rawValue ], request: &request)
        self.addAdditionalHeaders(route.headers, request: &request)
        self.addAdditionalHeaders(type(of: route).sharedHeaders, request: &request)
        do {
            let encoder = JSONEncoder()
            encoder.keyEncodingStrategy = route.keyEncodingStrategy
            request.httpBody = try encoder.encode(route.body)
            return request
        } catch {
            throw error
        }
    }

    
    private func addAdditionalHeaders(_ additionalHeaders: [String: String]?,request: inout URLRequest )
    {
        guard let additionalHeaders = additionalHeaders else { return }
        for (key, value) in additionalHeaders {
            request.setValue(value, forHTTPHeaderField: key)
        }
    }
}
