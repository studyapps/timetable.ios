//
//  DefaultGateway+IRouter.swift
//  ApplicationService
//
//  Created by Metalluxx on 28/09/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation

public protocol RequestExecutable: class {
    
    /// Build request and start REST default task
    ///
    /// - Parameters:
    ///   - route: request params
    ///   - completion: handler whith the running after get response
    /// - Returns: builded URLDataSessionTask
    @discardableResult
    func dataRequest(
        _ route: APIStructure,
        completion: @escaping (Result<Data, Error>, URLResponse?) -> Void
        ) -> URLSessionDataTask?
    
    /// Build request and start REST default task
    ///
    /// - Parameters:
    ///   - route: request params
    ///   - completion: handler whith the running after get data
    /// - Returns: builded URLDownloadSessionTask
    @discardableResult
    func downloadRequest(
        _ route: APIStructure,
        completion: @escaping (Result<URL, Error>, URLResponse?) -> Void
        ) -> URLSessionDownloadTask?
}

