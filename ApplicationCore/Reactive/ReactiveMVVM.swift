//
//  ReactiveMVVM.swift
//  RxSwiftTest
//
//  Created by Араик Гарибян on 18/09/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation
import RxSwift

public protocol ReactiveFlow {
    associatedtype Input
    associatedtype Output
}

public protocol ReactiveViewModel {
    associatedtype Input
    associatedtype Output
    
    func transform(input: Input, bag: DisposeBag) -> Output
    init()
}

public protocol ReactiveViewController {
    associatedtype Input
    associatedtype Output
    
    var bag: DisposeBag { get }
    var input: Input { get }
    func bind(output: Output)
}
