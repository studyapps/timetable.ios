//
//  SeconLeftString.swift
//  CoreArcService
//
//  Created by Metalluxx on 09/05/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation

public extension String {
    init(secondsLeft: UInt) {
        var second = secondsLeft
        var minute = 0
        var hour = 0
        var day = 0

        while second > 60 {
            second -= 60
            minute += 1
        }
        while minute > 60 {
            minute -= 60
            hour += 1
        }
        while hour > 24 {
            hour -= 24
            day += 1
        }
        self =  "\(day)d \(hour)h \(minute)m \(second)s"
    }
}
