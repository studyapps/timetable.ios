//
//  MjpegStreamLib.swift
//  Pods
//
//  Created by Kuray ÖĞÜN on 24/08/2017.
//
//

import UIKit

/// MJEG stream helper with UIImageView bindings
open class MJPEGStream: NSObject, URLSessionDataDelegate {

    // MARK: - Status imp
    private enum State {
        case stop
        case loading
        case play
        case backgroundWait
    }
    
    // MARK: - Services var
    private var receivedData: Data?
    private var dataTask: URLSessionDataTask?
    private var session: URLSession!
    open var contentRequest: URLRequest?
    
    // MARK: - Status var with default value
    /// Status working
    private var status: State = .stop

    // MARK: - isStarted bindings
    private(set) public var isStartedStream = false
    public var onStartedShowingStream:(() -> Void)?
    
    // MARK: - View var
    open var imageView: UIImageView

    
    // MARK: - Init
    public init(imageView: UIImageView) {
        self.imageView = imageView
        super.init()
        self.session = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: nil)

        NotificationCenter
            .default
            .addObserver(
                self,
                selector: #selector(eventToBackground),
                name: UIApplication.didEnterBackgroundNotification,
                object: nil
        )
        NotificationCenter
            .default
            .addObserver(
                self,
                selector:
                #selector(eventToForeground),
                name: UIApplication.willEnterForegroundNotification,
                object: nil
        )
    }

    public convenience init(imageView: UIImageView, contentRequest: URLRequest) {
        self.init(imageView: imageView)
        self.contentRequest = contentRequest
    }

    deinit {
        dataTask?.cancel()
    }

    // MARK: - Control
    /// Start showing stream with new URLRequest
    ///
    /// - Parameter urlRequest: request from which data will be loaded
    open func play(urlRequest: URLRequest) {
        if status == .play || status == .loading {
            stop()
        }
        contentRequest = urlRequest
        play()
    }

    /// Start showing stream with local URLRequest
    open func play() {
        if let request = contentRequest, (status == .stop)||(status == .backgroundWait) {
            status = .loading
            receivedData = Data()
            dataTask = session.dataTask(with: request)
            dataTask?.resume()
            return
        }
    }

    open func stop() {
        status = .stop
        dataTask?.cancel()
        isStartedStream = false
    }

    
    // MARK: - URL*Delegate's
    open func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        if let imageData = receivedData, imageData.count > 0,
            let receivedImage = UIImage(data: imageData as Data) {
            if status == .loading {
                status = .play
            }
            DispatchQueue.main.async { self.imageView.image = receivedImage }
            if !isStartedStream {
                onStartedShowingStream?()
                isStartedStream = true
            }
            
        }

        receivedData = Data()
        completionHandler(.allow)
    }

    open func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        receivedData?.append(data)
    }
}



// MARK: - Support vievcycle
extension MJPEGStream {
    @objc func eventToBackground() {
        if status != .stop {
            self.stop()
            status = .backgroundWait
        }
    }

    @objc func eventToForeground() {
        if status == .backgroundWait {
            self.play()
        }
    }
}
